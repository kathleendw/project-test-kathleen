import React, { useState, useEffect } from 'react';
import '../styles/Header.css';
import logo from '../assets/Logo.PNG'; 

const Header = () => {
  const [showHeader, setShowHeader] = useState(true);
  const [lastScrollY, setLastScrollY] = useState(0);

  const controlHeader = () => {
    if (window.scrollY > lastScrollY) {
      setShowHeader(false);
    } else {
      setShowHeader(true);
    }
    setLastScrollY(window.scrollY);
  };

  useEffect(() => {
    window.addEventListener('scroll', controlHeader);
    return () => {
      window.removeEventListener('scroll', controlHeader);
    };
  }, [lastScrollY]);

  return (
    <header className={`header ${showHeader ? 'show' : 'hide'}`}>
      <div className="logo">
        <img src={logo} alt="Suitmedia Logo" />
      </div>
      <nav>
        <ul>
          <li><a href="https://www.linkedin.com/in/kathleen-daniella-wijaya-27631b233">Work</a></li>
          <li><a href="https://www.linkedin.com/in/kathleen-daniella-wijaya-27631b233">About</a></li>
          <li><a href="https://www.linkedin.com/in/kathleen-daniella-wijaya-27631b233">Services</a></li>
          <li className="active"><a href="https://www.linkedin.com/in/kathleen-daniella-wijaya-27631b233">Ideas</a></li>
          <li><a href="https://www.linkedin.com/in/kathleen-daniella-wijaya-27631b233">Careers</a></li>
          <li><a href="https://www.linkedin.com/in/kathleen-daniella-wijaya-27631b233">Contact</a></li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
