import React from 'react';
import first from '../assets/chevron-left-solid.svg';
import previous from '../assets/angles-left-solid.svg';


const Pagination = ({ page, setPage, totalPages }) => {
  const generatePageNumbers = (currentPage) => {
    const pageNumbers = [];
    if (totalPages <= 5) {
      for (let i = 1; i <= totalPages; i++) {
        pageNumbers.push(i);
      }
    } else {
      if (currentPage <= totalPages - 5) {
        pageNumbers.push(currentPage, currentPage+1, currentPage+2, '...', totalPages);
      } else {
        pageNumbers.push(totalPages-4, totalPages-3, totalPages-2, totalPages-1, totalPages);
      }
    }
    return pageNumbers;
    };
    
  const visiblePages = generatePageNumbers(page);

  return (
    <div className="pagination">
      <img
        src={first}
        alt="First"
        onClick={() => {
          if (page !== 1) {
            setPage(1);
          }
        }}
        style={{ 
            width: '11px', 
            padding: '10px', 
            cursor: page === 1 ? 'not-allowed' : 'pointer', 
            pointerEvents: page === 1 ? 'none' : 'auto', 
            opacity: page === 1 ? 0.4 : 1 
        }}
      />
      <img
        src={previous}
        alt="Previous"
        onClick={() => {
          if (page !== 1) {
            setPage(page - 1);
          }
        }}
        style={{ 
            width: '20px', 
            padding: '10px', 
            cursor: page === 1 ? 'not-allowed' : 'pointer', 
            pointerEvents: page === 1 ? 'none' : 'auto', 
            opacity: page === 1 ? 0.4 : 1 
        }}
      />
      {visiblePages.map((pageNumber, index) => (
        <span 
            key={index}
            onClick={() => {
            if (typeof pageNumber === 'number') {
                setPage(pageNumber);
            }
            }}
            className={`page-number ${pageNumber === page ? 'active' : ''}`}
            style={{ cursor: 'pointer', borderRadius: '30%', padding: '15px 18px 15px 18px', backgroundColor: pageNumber === page ? '#ED6A32' : 'transparent', color: pageNumber === page ? 'white' : 'black' }}>
          {pageNumber}
        </span>
      ))}
      <img
        src={previous}
        alt="Next"
        onClick={() => {
          if (page !== totalPages) {
            setPage(page + 1);
          }
        }}
        style={{ 
            transform: 'rotate(180deg)', 
            width: '20px', 
            padding: '10px', 
            cursor: page === totalPages ? 'not-allowed' : 'pointer', 
            pointerEvents: page === totalPages ? 'none' : 'auto', 
            opacity: page === totalPages ? 0.4 : 1 
        }}
      />
      <img
        src={first}
        alt="Last"
        onClick={() => {
          if (page !== totalPages) {
            setPage(totalPages);
          }
        }}
        style={{ 
            transform: 'rotate(180deg)',
            width: '11px', padding: '10px', 
            cursor: page === totalPages ? 'not-allowed' : 'pointer', 
            pointerEvents: page === totalPages ? 'none' : 'auto', 
            opacity: page === totalPages ? 0.4 : 1 
        }}
      />
    </div>
  );
};

export default Pagination;
