import React from 'react';

const PageSizeOptions = ({ pageSize, setPageSize }) => {
  return (
    <label> Show per page:
      <select className="dropdown" value={pageSize} onChange={(e) => setPageSize(Number(e.target.value))}>
        <option value={10}>10</option>
        <option value={20}>20</option>
        <option value={50}>50</option>
      </select>
    </label>
  );
};

export default PageSizeOptions;
