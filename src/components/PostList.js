import React from 'react';
import PostCard from './PostCard';
import Pagination from './Pagination';
import SortOptions from './SortOptions';
import PageSizeOptions from './PageSizeOptions';

const PostList = ({ posts, page, pageSize, sortOrder, setPage, setPageSize, setSortOrder, totalPosts, totalPages }) => {
    const startIndex = (page - 1) * pageSize + 1;
    const endIndex = Math.min(page * pageSize, totalPosts);
  
    return (
    <div className="post-list">
      <div className="info-controls">
        <div className="info">
            Showing {startIndex} - {endIndex} of {totalPosts}
        </div>
        <div className="controls">
            <PageSizeOptions className="pageSize" pageSize={pageSize} setPageSize={setPageSize} />
            <SortOptions className="sort" sortOrder={sortOrder} setSortOrder={setSortOrder} />
        </div>
      </div>
      <div className="posts">
        {posts.map(post => (
          <PostCard key={post.id} post={post} />
        ))}
      </div>
      <Pagination page={page} setPage={setPage} totalPages={totalPages} />
    </div>
  );
};

export default PostList;
