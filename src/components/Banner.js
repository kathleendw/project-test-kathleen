import React, { useState, useEffect } from 'react';
import { Parallax } from 'react-parallax';
import '../styles/Banner.css';

const Banner = () => {
  const [bannerImage, setBannerImage] = useState('');

  useEffect(() => {
    const imageUrl = 'https://storage.googleapis.com/suitmedia/banner-image.png';
    setBannerImage(imageUrl);
  }, []);

  return (
    <div className="banner-container">
      <div className="banner-background">
        <Parallax bgImage={bannerImage} strength={500} className="banner">
          {/* Empty content just for background parallax */}
        </Parallax>
      </div>
      <div className="banner-content">
        <h1>Ideas</h1>
        <p>Where all our great things begin</p>
      </div>
    </div>
  );
};

export default Banner;
