import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

const getFormattedDate = (date) => {
    const options = { day: '2-digit', month: 'long', year: 'numeric' };
    return new Date(date).toLocaleDateString('en-GB', options);
  };

const PostCard = ({ post }) => {
  return (
    <div className="post-card">
      <div className="img-container">
        <img className="post-img" src={post.medium_image[0].url} alt={post.title} loading="lazy"/>
      </div>
      <div className="post-details">
        <p>{getFormattedDate(post.published_at)}</p>
        <h4>{post.title}</h4>
      </div>
    </div>
  );
};

export default PostCard;