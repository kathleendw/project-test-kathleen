import React from 'react';

const SortOptions = ({ sortOrder, setSortOrder }) => {
  return (
    <label> Sort by:
      <select className="dropdown" value={sortOrder} onChange={(e) => setSortOrder(e.target.value)}>
        <option value="-published_at">Newest</option>
        <option value="published_at">Oldest</option>
      </select>
    </label>
  );
};

export default SortOptions;
