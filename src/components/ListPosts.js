import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PostList from '../components/PostList';
import '../styles/ListPosts.css';

const ListPosts = () => {
  const [posts, setPosts] = useState([]);
  const [page, setPage] = useState(() => parseInt(localStorage.getItem('currentPage'), 10) || 1);
  const [pageSize, setPageSize] = useState(() => parseInt(localStorage.getItem('pageSize'), 10) || 10);
  const [sortOrder, setSortOrder] = useState(localStorage.getItem('sortOrder') || '-published_at');
  const [totalPages, setTotalPages] = useState(1);
  const [totalPosts, setTotalPosts] = useState(0);

  useEffect(() => {
    fetchPosts();
  }, [page, pageSize, sortOrder]);

  const fetchPosts = async () => {
    try {
      const response = await axios.get('https://suitmedia-backend.suitdev.com/api/ideas', {
        params: {
          'page[number]': page,
          'page[size]': pageSize,
          append: ['small_image', 'medium_image'],
          sort: sortOrder,
        },
      });
      setPosts(response.data.data);
      setTotalPages(Math.ceil(response.data.meta.total / pageSize));
      setTotalPosts(response.data.meta.total);
    } catch (error) {
      console.error('Error fetching posts:', error);
    }
  };

  useEffect(() => {
    localStorage.setItem('currentPage', page);
    localStorage.setItem('pageSize', pageSize);
    localStorage.setItem('sortOrder', sortOrder);
  }, [page, pageSize, sortOrder]);

  return (
    <div className="ListPosts">
      <PostList
        posts={posts}
        page={page}
        pageSize={pageSize}
        sortOrder={sortOrder}
        setPage={setPage}
        setPageSize={setPageSize}
        setSortOrder={setSortOrder}
        totalPages={totalPages}
        totalPosts={totalPosts}
      />
    </div>
  );
};

export default ListPosts;