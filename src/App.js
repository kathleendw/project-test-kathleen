import React from 'react';
import Header from './components/Header';
import Banner from './components/Banner';
import ListPosts from './components/ListPosts';

function App() {
  return (
    <div className="App">
      <Header />
      <Banner />
      <ListPosts />
      <main>
        <section style={{ height: '200vh' }}>
        </section>
      </main>
    </div>
  );
}

export default App;
